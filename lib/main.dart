import 'package:flutter/material.dart';
import 'package:randomuser/presentation/screens/random_user_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: Colors.deepPurple, background: const Color(0xff68B1C9)),
        useMaterial3: true,
      ),
      home: const RandomUserScreen(),
    );
  }
}
