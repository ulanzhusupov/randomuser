import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s16W500 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s16W400 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s12W400 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
  );
}
