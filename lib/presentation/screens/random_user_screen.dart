import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:randomuser/core/network/dio_settings.dart';
import 'package:randomuser/data/models/random_user_model.dart';
import 'package:randomuser/presentation/theme/app_fonts.dart';
import 'package:randomuser/presentation/widgets/custom_textfield.dart';
import 'package:url_launcher/url_launcher.dart';

class RandomUserScreen extends StatefulWidget {
  const RandomUserScreen({super.key});

  @override
  State<RandomUserScreen> createState() => _RandomUserScreenState();
}

class _RandomUserScreenState extends State<RandomUserScreen> {
  RandomUserModel model = RandomUserModel();

  Future<void> getRandomUser() async {
    final Dio dio = DioSettings().dio;
    try {
      final Response response = await dio.get("https://randomuser.me/api/");
      model = RandomUserModel.fromJson(response.data);
      setState(() {});
    } catch (e) {
      print(e);
    }
  }

  Future<void> launchURL(String cityName) async {
    final Uri url = Uri.parse('https://maps.google.com?q=$cityName');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              getRandomUser();
            },
            child: const Text(
              "Generate",
              style: AppFonts.s16W500,
            ),
          ),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: FutureBuilder(
              future: getRandomUser(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Column(
                    children: [
                      const SizedBox(height: 30),
                      CircleAvatar(
                        backgroundColor: Colors.amber,
                        radius: 74,
                        backgroundImage: NetworkImage(
                            model.results?.first.picture?.large ??
                                "http://via.placeholder.com/350x150"),
                      ),
                      const SizedBox(height: 30),
                      CustomTextField(
                          labelText: "Name",
                          hintText:
                              "${model.results?.first.name?.title} ${model.results?.first.name?.first} ${model.results?.first.name?.last}"),
                      const SizedBox(height: 24),
                      CustomTextField(
                        labelText: "Username",
                        hintText: "${model.results?.first.login?.username}",
                      ),
                      const SizedBox(height: 24),
                      CustomTextField(
                        labelText: "Phone number",
                        hintText: "+${model.results?.first.phone}",
                      ),
                      const SizedBox(height: 24),
                      CustomTextField(
                        labelText: "Email",
                        hintText: "${model.results?.first.email}",
                      ),
                      const Spacer(),
                      SizedBox(
                        width: double.infinity,
                        height: 48,
                        child: ElevatedButton(
                          onPressed: () {
                            launchURL(
                                model.results?.first.location?.city ?? "");
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: const Color(0xff263775),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          child: Text(
                            "Get location",
                            style:
                                AppFonts.s16W500.copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                      const SizedBox(height: 24),
                    ],
                  );
                } else {
                  return const CircularProgressIndicator.adaptive();
                }
              }),
        ),
      ),
    );
  }
}
