import 'package:flutter/material.dart';
import 'package:randomuser/presentation/theme/app_fonts.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.labelText,
    required this.hintText,
  });

  final String labelText;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return TextField(
      readOnly: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        fillColor: Colors.white,
        filled: true,
        labelText: labelText,
        labelStyle: AppFonts.s12W400,
        alignLabelWithHint: true,
        floatingLabelStyle: AppFonts.s12W400,
        floatingLabelAlignment: FloatingLabelAlignment.start,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        hintText: hintText,
        hintStyle: AppFonts.s16W400,
        border: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xff4F4F51),
            width: 1.5,
          ),
        ),
      ),
    );
  }
}
